//  Copyright (c) 2017 Markus Jylhänkangas
#include "database.hpp"

// TODO: Vaihda nämä parempaan
std::string encrypt(std::string msg, std::string const &key) {
  if (!key.size())
    return msg;

  for (std::string::size_type i = 0; i < msg.size(); ++i)
    msg[i] ^= key[i % key.size()];
  return msg;
}

std::string decrypt(std::string const &msg, std::string const &key) {
  return encrypt(msg, key);
}

const std::string TempString =
    "mSjhXOQMaI3AuxG3YSADfBmzeoRidWBqMeuKCHx5NSoFxeAl6vuznxqsoXL5yRZbbUwnN3Qdtf"
    "kUDjYxL9CCEdN22YK3QhdkdTO2i2xyZmxgZrx3L0jfz5EkyRuKJrfSt3RdO72eF4lxw1neChES"
    "L9vD0Tujsb3HKZG2SaeDeYPBnGAYKLsw62FRtqe7kLKrlvfap8yIdCCoYtdvdPDnQibUDwJaMT"
    "ZqCPN4fafo4d94aKZN6GurCQJvvotMlpsot589xGKYc2knvYxr39rALzi2UeeK4z2Qlj98Vxuy"
    "NkRlQr6d2zrDUN3GFyBz1ynHrASjqJNf6Ruh7Cck0t970VBvwyrVe5JIWI951L6pAaDkHkqTxT"
    "zJ05XBenbOiV3Uwlb8cg8qs72eHraGnBh8fVcyF2ltccqfoxtA3vLUJVqQk8yaDVrg8nSVvLme"
    "4WtoOr1kr6maSjwZJOz3SU9IT6A6qTJG4iYbYxLVSYzuhGkfBRxwmxhN6W6CPaXBFWEKghIVrT"
    "YufdSQmPsBmTN58QAaAvK9tgTeijAfzj0R1q7bP521yjhQcnpoMxvfRVlWC29EZsF9RTpkLY3d"
    "mZGMaXq4Kci1xBBCuKak8ajn0XoWYhDYEZlltmZ1HkudYVgxrrIVWG1JuzLyM7FyYvFd10XgMG"
    "cBB2vROoa27U1w9XvicUjARFK5Ov3Dm6hhAnNUD5Pr8LaPAV8junoviJPU7L94ugeaLFJRWkW2"
    "VInqXi0o4oa634RnVd6Z8MXCnTXrLnrZr5JgdBpLSUMDiq5HU6L2hVuC7f1Yd0sFRWwbXPRSjM"
    "4VtlnDZhM3mfH5p18aFajDmpZh8SZUvAympnl20N4fx1y0BtUpE3MTtlHvnk1o7rJ4HBuw9L5A"
    "CpJbOQgGdk4ybOKgedc3lNskplWiRwqzlvWjo6NmB3vRpBs8RKCMRlET2v58CSfog2SFx6wrtl"
    "JOzz1zfG8xsiVnaqO3eU3KQuUir8qQlLENpOfWeVjxLs2pUaCGbK3Eg3q5KzZYwAfAL4Ko4WsL"
    "MN3iKYcaYPiTMFwnVSuxMAxHqlRaJo1x3V70LScNjSoYdWCjmpPcCCp8Og3paWlsg0J88waEZ1"
    "tCBvRvnwwnu6mKQrWrX2lpwfiZJdqFZ8YAZfoSY6Kplo8lwGENRSD2e7yw9l8J8117wNGP5uvW"
    "WgXNF5XFchVLObOViAijHAdU5Gcwt4zcMTkOwcTEkOPnwJWPCL1UEiweXGMrMN29vmCu93uxja"
    "Em4n6Tf45PT2CkqX2u5BAYET3EEDte8uPBcIDIvS0VweY6dbnxmkBk7HjSDrgWkorZEOwRmKrD"
    "4uC1T1pGFIyN9jreJqW4lsA9VpohYsHpm4X8GJctth95YkqOWaGL5IK0mpE1OgwcTUo7gfKrnN"
    "o2DP2UntfELuMDkpAVV5U2OMYwabs8thA13j2ey46WLFtzY7yAA8a8gqP6fmuRgkHSSLwRWA6F"
    "1bIKkfTfT36C9Jx8JZWVrZNfWEnqUwnL4dmJwDvCOimmdoDDy74daZ9fSPvJwXHVnL0aSjTpre"
    "xPHcCHHSXxK9iycpKK59sXfA2kJ5sAghM7jOXWeFLBxodT07DZjqg1GCygjoGjHBObNDVvNQDP"
    "pMgrIvpgXLTLsrVIr6JhfuEYMI7pUhcvxi9br7F5n2kuhhQQoiR7Yi2OQZmi0Aqi5NMcZboW1V"
    "GjaYe6IDvtNVvY19OKNtErKISpVKT4iTdMeXD7gJ7fjZd1lTwek6Nv61iyT39W9S83SHaUKd19"
    "baEi6V7IeBiH2niBu11XCLV7Qkxk7cd0KkyZeaXvzvp3Gby0QgIZcvwU4Ot2AcJ4nhccqUBSIx"
    "WOsY1RCxTEmBlxKJmXHZfnU7QOUULQ5Jm3tXwVeY4x7N2VxGGsqveCKHRvp2XDNJ3XNvZMLCFT"
    "NPmZe5QhmOJ7w7GQuhQwO3t7Wf0ITTRUTEwXfkn4xfujnEnZML6Gcgk2fo9b5LbKsfkCWsL7GE"
    "YT2UXCwzlACQfOMgQwGurknfupSsD0ihBmMsPsy8TwpLsUU0ckAIw0XGazO7eKRa3TpxT7an6Y"
    "QrKjVvva40DPrFf6mug9bVucdmc8KtYWkq2z4YlFh3NASD24i4inpAGn4Uvakv8nyndx09y1Vh"
    "PhqzohcJ8CxfdbrtLbYSdk92FtGrDIjsVQR2apN9FOwQfav3e7ZmFCuT4WR6H9Ieob4P5Nrauw"
    "GmAmGR5KahBZXLmHhcIGuGvthtYzUusCQdSdxjTla80zkewXY3oKEZWzyFNPoc4deu29Mi6La1"
    "l1BMnLhAozJfdi2jx9UWmVz9OtEgAOeVMrIFszxpTclbHVyYwYQhh6cimkhhQPJ5v5vnd0nNjN"
    "2jlYhePUAaH0cS1ruoKQNKp7c0tdWEBfD3X24rKGphEqxwa7xIPUUwM7oTrvvUBDIh7NOKGh0E"
    "DgWdE77ecds6dcF4i1dGw7VuA50jFoUR5rseWm0tPoA7goRyTRsRMlOCftV3daYlEjUwD9vfmt"
    "uOdEnWxyR7UbzU63AID4IoV9cx1BArvdWlV3vQd8XwBOZOX4DHSfaJHnuSjs7Nh2HkeHVKgVFz"
    "CEnQAJtnzr0ibYMT3AXgcdPd9cmnqpFnD20cyoH1v1wBJkoEHyLTdU93h9xa1IkkiHZJpN9puQ"
    "nNPok71dHFc91KYcQRY2uakHX7hyN9YHFhX35fjGVJuxfYtZw69VmCQ3zfSvbk9sih0fZ9uvhd"
    "qYMetILW1pnCWXsXQJmVjzKp7hMpAW5k8ZrZmGj2gVVrmNCMYjM8Vk7oxLVDV21V2wdrANFv3e"
    "vzCJheCwWILvc3XSYq6g8yFD0V3YGrynTA4JrpxxrTGxVrJF0cAJIcOJcF40i0OswWkHvNZAP2"
    "bDrYfaPLGa9uwAk4WLfea8kw9pFam6Ypl9502US8XD4NzZclnbQWjr3yP8dz1N9mM7wsYDq4mi"
    "DmwV8CzMrvyeGAnGgPHuK0S2H7G6u0xNp5GOZovIrPVpAXfxnC4DiSiwuhaCINUq9Ok1TK9c1i"
    "CS1v9HX8uZHTRhtCuSlEmcg5g90dGAfVjxUeGOapLvtaToFe0nHgmYj34zb8n0Huj9Jkkbo5GE"
    "ent6tvspaG2IPDqdaFYILUkm1GoeulwhNRspuHTAaQdLim6ZhJPdwJBIeH2rqo7glaHahG1R4m"
    "rnMih2OplVO9bRCDfjGqkFKUzPMTZWn0hx9TumXDORZIq2fEpaeLPggVfTsSrNjMIMnpm07dcw"
    "LUCyAq0Hyzs66f1KjkncKImz1dqoZbCUEp3WsXmTToIjIwWBeWjfYnbOaFgsa7jKvEk6NMFE6G"
    "1E2MRUhHxUqDwYaa5PFTvBqKKId1asmA2QHSYAU4j";

QDataStream &operator>>(QDataStream &in, ProductStruct &product) {
  in >> product.NameProd;
  in >> product.PriceProd;
  in >> product.KplProd;
  return in;
}

QDataStream &operator<<(QDataStream &out, const ProductStruct &product) {
  out << product.NameProd;
  out << product.PriceProd;
  out << product.KplProd;
  return out;
}

// TODO: kryptaa asiakkaat
QDataStream &operator>>(QDataStream &in, CustomerStruct &customer) {
  in >> customer.Nimi1;
  in >> customer.Nimi2;
  in >> customer.Puh;
  in >> customer.Katu1;
  in >> customer.Katu2;
  in >> customer.Kaupunki;
  in >> customer.Postinum;
  in >> customer.Potilas;
  return in;
}

QDataStream &operator<<(QDataStream &out, const CustomerStruct &customer) {
  out << customer.Nimi1;
  out << customer.Nimi2;
  out << customer.Puh;
  out << customer.Katu1;
  out << customer.Katu2;
  out << customer.Kaupunki;
  out << customer.Postinum;
  out << customer.Potilas;
  return out;
}

// kryptaa firmantiedot
QDataStream &operator>>(QDataStream &in, CompanyStruct &company) {
  in >> company.Address;
  in >> company.City;
  in >> company.Iban;
  in >> company.Name;
  in >> company.Phonenumber;
  in >> company.Postal;
  in >> company.Y_tunnus;
  in >> company.SaveFolderPath;
  in >> company.Maksuaika;
}

QDataStream &operator<<(QDataStream &out, const CompanyStruct &company) {
  out << company.Address;
  out << company.City;
  out << company.Iban;
  out << company.Name;
  out << company.Phonenumber;
  out << company.Postal;
  out << company.Y_tunnus;
  out << company.SaveFolderPath;
  out << company.Maksuaika;
}

QDataStream &operator>>(QDataStream &in, DatabaseStruct &data) {
  in >> data.ProductList;
  in >> data.CustomerList;
  in >> data.CompanyInfo;
  in >> data.InvoiceNumber;
  return in;
}

QDataStream &operator<<(QDataStream &out, const DatabaseStruct &data) {
  out << data.ProductList;
  out << data.CustomerList;
  out << data.CompanyInfo;
  out << data.InvoiceNumber;
  return out;
}
